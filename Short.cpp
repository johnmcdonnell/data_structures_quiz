/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

#include "Short.h"

using namespace std;

Short::Short()
{

}

Short::Short(string question, string correct_answer)
{
	this->question = question;
	this->correct_answer = correct_answer;
}

string Short::get_correct_answer()
{
	return this->correct_answer;
}

string Short::get_questions()
{
	return this->question;
}

string Short::get_question_type()
{
	return "short";
}

Short::~Short()
{

}