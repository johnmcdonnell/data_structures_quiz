/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

#include <string>
#include "ResizeableArray.h"
#include "Question.h"
#include <iostream>
using namespace std;

class Quiz
{
public:

	string quiz_title;

	ResizeableArray<Question>* quiz_questions;
	Quiz(void);

	Quiz(string quiz_title, ResizeableArray<Question>* quiz_questions);

	string get_quiz_title();

	ResizeableArray<Question>* get_questions();

	~Quiz();
};