/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

#pragma once
template<class T>
class DLinkedList {

public:
	DListNode<T> *head, *tail;
	int count;

	DLinkedList() {
		head = 0; // null, so nothing in list
		tail = 0;
		count = 0;
	}
	void prepend(T data) {
		DListNode<T> *new_head = new DListNode<T>();
		new_head->data = data;
		new_head->next = head;
		// set the current head node's previous pointer to the new node
		head->previous = new_head;
		head = new_head;
		// deal with the case where this is the only node
		if (0 == tail) {
			tail = head;
		}
		count++;
	}
	void append(T data) {
		// is there actually a last node?

		if (0 == head) {
			// create a new head node

			head = tail = new DListNode<T>;
			head->data = data;
		}
		else {
			// insert a new node after the tail

			tail->insertAfter(data);

			// update the tail pointer
			tail = tail->next;
		}
		count++;
	}

	void removeHead() {
		DListNode<T> *old_head = head;
		head = head->next;
		delete old_head;
		old_head = 0;
		//head->previous = 0; // new head's previous pointer should be null
		count--;
	}

	void removeTail()
	{
		if (tail == head)
		{
			delete head;
			head = tail = 0;

		}
		else
		{
			DListNode<T> *old_tail = tail;
			tail = old_tail->previous;
			delete old_tail;
			old_tail = 0;
			tail->next = 0; // new tail's previous pointer should be null
		}
		count--;
	}
	// destructor iterates through the list,
	// deleting all items.
	~DLinkedList() {
		DListNode<T> *iterator, *next;
		iterator = head;
		while (iterator != 0) {
			// save pointer to the next node
			next = iterator->next;
			// delete the current node
			delete iterator;
			// make the next node the current one
			iterator = next;
		}
	}

	DListIterator<T> getIterator() {
		return DListIterator<T>(this, head);
	}

	void remove(DListIterator<T> &iterator) {
		if (iterator.list != this) {
			return;
		}

		if (!iterator.isValid()) {
			return;
		}

		// easy case: the head node is to be removed:
		if (iterator.node == head) {
			iterator.advance();
			removeHead();
			count--;
			return;
		}

		/* otherwise,
		scan through the list to get the node prior to
		the one selected by the iterator paramter.
		*/
		DListIterator<T> scanner = getIterator();
		while (scanner.node->next != iterator.node) {
			scanner.advance();
		}

		/*
		if the node to be deleted is the tail, we need
		to update the tail pointer.
		*/
		if (scanner.node->next == tail) {
			tail = scanner.node;
		}

		// re-link the list
		scanner.node->next = iterator.node->next;
		iterator.node->next->previous = scanner.node;

		// now do the deletion
		delete iterator.node;
		count--;

	}

};