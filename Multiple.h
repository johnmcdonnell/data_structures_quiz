/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

#include <string>
#include "Question.h"
#include <iostream>
using namespace std;

class Multiple: public Question
{
private:
	
	string correct_answer;
	string option_one;
	string option_two;
	string option_three;
	string option_four;

public:

	Multiple(string question, string correct_answer, string option_one, string option_two, string option_three, string option_four);

	Multiple();
	
	string get_questions();
	string get_correct_answer();
	string get_option_one();
	string get_option_two();
	string get_option_three();
	string get_option_four();
	string get_question_type();

	~Multiple();
};