/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

/*
For this assignment i started out using arrays but after awhile i was unable to get anything working. I mananged to store the quizes into an 
array but was unable to extract the information out when needed.
I then came up with the soultion i have now. A hashtable which stores an int(key) and questions (a resizeable array).
I search the file for "quiz", then check the quiz type, store it into the array giving it a value in the hashtable.
I use polymorphism for my questions, short, true or false and mulitple all inherit from question. I felt this was the best way to split 
all questions up and be table to access each when needed.
*/

//include files
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <istream>
#include "ResizeableArray.h"
#include "DLinkedListHeader.h"
#include "HashTable.h"
#include "Quiz.h"
#include "Multiple.h"
#include "Question.h"
#include "Short.h"
#include "TrueFalse.h"
using namespace std;

//gobal variables
int hash_function(int value);
int number_of_quizes = 0;
int players_score = 0;


// gobal methods
void load_quizes(ifstream &quiz_file);
void load_questions(ifstream &quiz_file, int selected_quiz);
void print_out_all_quiz_questions();
bool is_equals(const string& a, const string& b);

//gobal array and hashtable
ResizeableArray<string> quiz_array;
HashTable<int, Question*> quiz_questions(40, &hash_function);

int hash_function(int value)
{
	return value % 20;
}

bool is_equals(const string& a, const string& b)
{
	unsigned int sz = a.size();
	if (b.size() != sz)
	{

		return false;
	}
	for (unsigned int i = 0; i < sz; ++i)
	{

		if (tolower(a[i]) != tolower(b[i]))
		{
			return false;
		}
	}
	return true;
}

//printing out all the quiz questions.
void print_out_all_quiz_questions()
{
	//extracts characters from the input sequence and discards them
	cin.ignore();

	for (int i = 0; i < quiz_questions.count(); i++)
	{
		//hashentry contains Key and array of questions.
		DListIterator<HashEntry<int, Question*>> iter = (quiz_questions.m_table[i].getIterator());

		while (iter.isValid())
		{
			string players_answer;

			HashEntry<int, Question*> item = iter.item();

			cout << item.m_data->get_questions() << endl << endl;

			//getting multiple type questions. questions use inheritance
			if (item.m_data->get_question_type() == "multiple")
			{
				// cout << "test --------- inside" << endl;
				cout << "(1) " << item.m_data->get_option_one() << endl << endl;
				cout << "(2) " << item.m_data->get_option_two() << endl << endl;
				cout << "(3) " << item.m_data->get_option_three() << endl << endl;
				cout << "(4) " << item.m_data->get_option_four() << endl << endl;
			}

			cout << endl;

			getline(cin, players_answer);

			if (is_equals(item.m_data->get_correct_answer(), players_answer))
			{
				cout << endl << endl;

				cout << "Great Answer is Correct!" << endl << endl;

				players_score++;
			}
			else
			{
				cout << endl << endl;
				cout << "Sorry That Answer is Incorrect!" << endl << endl;
			}

			iter.advance();
		}
	}
}

// loadinf quiz method, taking in the quiz file.
void load_quizes(ifstream &quiz_file)
{
	//variables
	string line = "";
	string cell_value;
	string quiz_title;
	int quiz_topic = 1;

	 
	//find the word "quiz" in the file, from there push it into the array and store for later use.
	while (getline(quiz_file, line))
	{
		stringstream lineStream(line);

		getline(lineStream, cell_value, ',');

		if (cell_value == "Quiz")
		{	
			getline(lineStream, quiz_title, ',');

			//push information into the array
			quiz_array.push(quiz_title);

			number_of_quizes++;
		}
	}

	//displaying the different types of quizes that are stored in the array.
	for (int i = 0; i < number_of_quizes; i++)
	{
		cout << quiz_topic << ") Press " << quiz_topic << " To Play The " << quiz_array.getElement(i) << endl;

		quiz_topic++;
	}

	cout << endl;
}

//loading questions method, taking in the file and the users selected quiz.
void load_questions(ifstream &quiz_file, int selected_quiz)
{
	cout << endl << endl;

	//variables
	string line = "";
	int f = 0;
	int question_number = 0;

	//setting the selected quiz number to the current quiz.
	int quiz_number = selected_quiz;
	
	//seeking from the start to the end of the quiz.
	quiz_file.clear();
	quiz_file.seekg(0, quiz_file.beg);


	while (getline(quiz_file, line))
	{
		//variables
		stringstream lineStream(line);
		string cell_value;
		string quiz;
		string questions;

		while (getline(lineStream, cell_value, ','))
		{
			//find the word quiz in the file.
			if (cell_value == "Quiz")
			{
				f++;
				getline(lineStream, cell_value, ',');
			}

			// if the quiz_number == 0 then process.
			if (f == quiz_number)
			{
				if(is_equals(cell_value, "short"))
				{
					//variables
					string question;
					string correct_answer;

					//getting the info from each line.
					getline(lineStream, question, ',');
					getline(lineStream, correct_answer, ',');

					Short* short_quiz = new Short(question, correct_answer);

					//inserting them into the hashtable
					quiz_questions.Insert(question_number, short_quiz);

					question_number++;
				}

				if(is_equals(cell_value, "truefalse"))
				{
					//variables
					string question;
					string correct_answer;

					//getting the info from each line.
					getline(lineStream, question, ',');
					getline(lineStream, correct_answer, ',');
					
					TrueFalse* true_false_quiz = new TrueFalse(question, correct_answer);
					
					//inserting them into the hashtable
					quiz_questions.Insert(question_number, true_false_quiz);

					question_number++;
				}

				//does the cell contain "multiple"
				if (is_equals(cell_value, "multiple"))
				{

					//variables
					string question;
					string correct_answer;
					string option_one;
					string option_two;
					string option_three;
					string option_four;

					//getting the info from each line.
					getline(lineStream, question, ',');
					getline(lineStream, correct_answer, ',');
					getline(lineStream, option_one, ',');
					getline(lineStream, option_two, ',');
					getline(lineStream, option_three, ',');
					getline(lineStream, option_four, ',');

					//creating a new multiple_quiz with all the required information.
					Multiple* multiple_quiz = new Multiple(question, correct_answer, option_one, option_two, option_three, option_four);

					//inserting them into the hashtable
					quiz_questions.Insert(question_number, multiple_quiz);

					question_number++;
				}
			}
		}
	}
}


// main method
int main()
{
	int players_selection = 0;
	string player_name = "";

	// loading in the quiz file
	ifstream quiz_file("quiz.csv");

	cout << "=================================================================== " << endl;
	cout << "============================ Geek Quiz ============================ " << endl;
	cout << "=================================================================== " << endl << endl;

	cout << "Please Enter Your Name " << endl << endl;
	cin >> player_name;
	cout << endl << endl;

	//process until user enters -1
	while (players_selection != -1)
	{
		
		//loading the quizes
		load_quizes(quiz_file);

		cout << "** To Exit This Application Press -1 ** " << endl << endl;
		cout << "Please Select Which Quiz You Would Like To Play " << endl << endl << endl;

		//taking players selection
		cin >> players_selection;

		//loading the questions for the quiz
		load_questions(quiz_file, players_selection);

		//printing all the correct quiz questions
		print_out_all_quiz_questions();

		//clearinging the question once answered.
		quiz_questions.ClearAll();

		//showing players score.
		cout << player_name << " Your Score is: " << players_score << endl << endl;
	}
}
