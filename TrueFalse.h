/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

#include <string>
#include "Question.h"
#include <iostream>
using namespace std;

class TrueFalse: public Question
{
private:

	string correct_answer;

public:

	TrueFalse(string question, string correct_answer);

	TrueFalse();

	~TrueFalse();

	string get_questions();

	string get_correct_answer();

	string get_question_type();
};