/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

#include "TrueFalse.h"

using namespace std;

TrueFalse::TrueFalse()
{

}

TrueFalse::TrueFalse(string question, string correct_answer)
{
	this->question = question;
	this->correct_answer = correct_answer;
}

string TrueFalse::get_correct_answer()
{
	return this->correct_answer;
}

string TrueFalse::get_questions()
{
	return this->question;
}

string TrueFalse::get_question_type()
{
	return "truefalse";
}

TrueFalse::~TrueFalse()
{

}