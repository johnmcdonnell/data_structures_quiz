/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/


#include "Multiple.h"
using namespace std;

Multiple::Multiple()
{

}

Multiple::Multiple(string question, string correct_answer, string option_one, string option_two, string option_three, string option_four)
{
	this->question = question;
	this->correct_answer = correct_answer;
	this->option_one = option_one;
	this->option_two = option_two;
	this->option_three = option_three;
	this->option_four = option_four;

}

string Multiple::get_correct_answer()
{
	return this->correct_answer;
}

string Multiple::get_questions()
{
	return this->question;
}

string Multiple::get_option_one()
{
	return this->option_one;
}

string Multiple::get_option_two()
{
	return this->option_two;
}

string Multiple::get_option_three()
{
	return this->option_three;
}

string Multiple::get_option_four()
{
	return this->option_four;
}

string Multiple::get_question_type()
{
	return "multiple";
}

Multiple::~Multiple()
{

}

