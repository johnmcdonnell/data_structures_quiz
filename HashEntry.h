/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

#include <iostream>

template <class KeyType, class DataType>
class HashEntry
{
	public: 

		KeyType m_key; 
		DataType m_data; 

};