/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

template<class T> class DListNode;

template<class T>
class DListNode {
    
public:
    T data;
    DListNode<T> *previous, *next;
    
    DListNode() {
        previous = next = 0;
    }
    
    void insertAfter(T newContent) {

        DListNode<T>* newNode = new DListNode<T>();
        newNode->data = newContent;
        
		  // point new node to current next node
        newNode->next = next;
        // make this node point to new node
        next = newNode;
		// make the new node point back to this node
		newNode->previous = this;
    }
};

