/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

#include <string>
#include <iostream>
using namespace std;

#pragma once

class Question 
{
protected:

	string question;
public:

	Question(void);
	
	Question(string question);
	string get_questions();

	//needed for inheritance and polymorphism
	virtual string get_correct_answer(){return "";};
	virtual string get_option_one(){return "";};
	virtual string get_option_two(){return "";};
	virtual string get_option_three(){return "";};
	virtual string get_option_four(){return "";};
	virtual string get_question_type(){return ""; };

	~Question();
};
