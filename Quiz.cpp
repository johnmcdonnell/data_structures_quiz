/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

#include "Quiz.h"

using namespace std;

Quiz::Quiz()
{

}

Quiz::Quiz(string quiz_title, ResizeableArray<Question>* quiz_questions)
{
	this->quiz_title = quiz_title;
	this->quiz_questions = quiz_questions;
}

string Quiz::get_quiz_title()
{
	return this->quiz_title;
}

ResizeableArray<Question>* Quiz::get_questions()
{
	return this->quiz_questions;
}

Quiz::~Quiz()
{

}