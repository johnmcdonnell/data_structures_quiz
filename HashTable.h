/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

#include <iostream>
#include "HashEntry.h"
#include "DLinkedList.h"
#include "ResizeableArray.h"

template <class KeyType, class DataType>
class HashTable
{
private:
	typedef HashEntry<KeyType, DataType> Entry; 
	int m_size; 
	int m_count; 
	
	int (*m_hash)(KeyType); 
public: 
		ResizeableArray< DLinkedList< Entry > > m_table; 
		HashTable(int size, int (*ptr)(KeyType)):m_table(size)
		{
			m_hash = ptr;
			m_size = size;
			m_count = 0;
			for(int i =0; i < size; i++)
			{
				m_table.push(*(new DLinkedList<Entry>()));
			}
			
		}
		void Insert( KeyType p_key, DataType p_data )
		{
			Entry entry;
			entry.m_data = p_data;
			entry.m_key = p_key;
			int index = m_hash( p_key ) % m_size;
			m_table[index].append( entry );
			m_count++;
		}
		Entry* Find( KeyType p_key ) 
		{ 
			int index = m_hash( p_key ) % m_size; 
			DListIterator<HashEntry<KeyType, DataType>> itr = m_table[index].getIterator(); 
			
			while( itr.isValid() ) 
			{ 
				if( itr.item().m_key == p_key ) 
					return &(itr.item()); 
				itr.advance(); 
			} 
		return 0; 
		}
		Entry* ClearAll()
		{
			for (int i = 0; i < m_count; i++)
			{
				
				int index = m_hash(i) % m_size;
				DListIterator<HashEntry<KeyType, DataType>> itr = m_table[index].getIterator();
				
				while (itr.isValid())
				{
					
					m_table[index].remove(itr);
					itr.advance();
				}
			}
			return 0;
		}
		bool Remove( KeyType p_key ) 
		{ 
			int index = m_hash( p_key ) % m_size; 
			DListIterator<Entry> itr = m_table[index].getIterator(); 
			while( itr.isValid() ) 
			{ 
				if( itr.item().m_key == p_key ) 
				{ 
					m_table[index].remove( itr ); 
					m_count--; 
					return true; 
				} 
				itr.advance(); 
			} 
			return false; 
		} 
		int count()
		{
			return m_count;
		}

};