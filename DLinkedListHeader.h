/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/
#pragma once

#ifndef DLINKEDLISTHEADER_H_
#define DLINKEDLISTHEADER_H_

// forward declare classes
template<class T> class DListNode;
template<class T> class DLinkedList;
template<class T> class DListIterator;

// include the class header files
// (with implementations)
#include "DListNode.h"
#include "DLinkedList.h"
#include "DListIterator.h"

#endif
