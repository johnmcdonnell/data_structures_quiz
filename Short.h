/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

#include <string>
#include "Question.h"
#include <iostream>


using namespace std;
class Short: public Question
{
private:

	string correct_answer;
public:

	Short(string question, string correct_answer);

	Short();

	~Short();

	string get_questions();

	string get_correct_answer();

	string get_question_type();


};