/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

#ifndef RESIZEABLEARRAY_H_
#define RESIZEABLEARRAY_H_

#include <cstdlib>
#include <iostream>

template<class T>
class ResizeableArray {
private:
	int grow_size;

	void resize()
	{
		size += grow_size;
		T* new_array = new T[size];

		for (int i = 0; i < nItems; i++)
		{
			new_array[i] = items[i];
		}
		items = new_array;
	}
	
	
public:
	T *items;
	int nItems;
	int size ;
	ResizeableArray() {
		grow_size = 3;
		items = new T[grow_size];
		nItems = 0;
		size = grow_size;
	}

	ResizeableArray(int gs) {
		grow_size = gs;
		items = new T[gs];
		nItems = 0;
		size = gs;
	}

	void push(const T& item) {

		int array_size = sizeof(items)/sizeof(T);

		// need to re-size ?
		if ( nItems == array_size ) {
			resize();
		}
		
		items[nItems] = item;
		nItems++;
	}

	int length() {
		return nItems;
	}

	T& getElement(int index) {
		if ( index < nItems ) {
			return items[index];
		} else {
			abort();
			
		}
	}

	T& operator[] ( int index ) {
		return getElement(index);
	}

	~ResizeableArray() {
		delete[] items;
	}
};

#endif /* RESIZEABLEARRAY_H_ */
