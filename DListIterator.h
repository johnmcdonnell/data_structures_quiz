/*
John McDonnell
D00096987
Computing Level 8
Year 2
Group 2
Data Structures for Games
4/05/2015
*/

template<class T> class DListIterator;

template<class T>
class DListIterator {
    
public:
    DListNode<T> *node;
    DLinkedList<T> *list;
    
    // constructor defaults to zero:
    // it'll never be called directly by the user
    // anyway.
    DListIterator(DLinkedList<T> *l = 0,
                  DListNode<T> *n = 0) {
        list = l;
        node = n;
    }
    
    // reset the iterator to the first node
    void start() {
        node = list -> head;
    }

	// reset the iterator to the last node
	void end() {
		node = list -> tail;
	}
    
    // get the current item
    T& item() {
        return node -> data;
    }
    
    // checks that we've another item ready
    bool isValid() {
        return ( 0 != node );
    }
    
    // advance to the next item
    void advance() {
        if ( 0 == node ) {
            return;
        }
        node = node -> next;
    }

	// go back to the previous item.
	void reverse() {
		if ( 0 == node ) {
			return;
		}
		node = node -> previous;
	}
    
};

